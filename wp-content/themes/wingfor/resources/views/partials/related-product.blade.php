<?php
	global $post;
	$terms = get_the_terms( $post->ID , 'product-category', 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
			'post_type' => 'products',
			'tax_query' => array(
								array(
										'taxonomy' => 'product-category',
										'field' => 'id',
										'terms' => $term_ids,
										'operator'=> 'IN'
								 )),
			'posts_per_page' => 3,
			'orderby' => 'date',
			'post__not_in'=>array($post->ID)
	 ) );
?>
<aside class="related product-content related-product">
	<div class="main-title"><a><h2><?php _e('Related products','wingfor'); ?></h2></a></div>
	<div class="related-product-content">
		<div class="row">

			<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>

				<article class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item">
					<figure>
						<a href="<?php the_permalink();?>">
							<img src="<?php echo wingfor_get_thumbnail_url('product'); ?>" alt="<?php the_title(); ?>" />
						</a>
					</figure>
					<div class="info">
						<div class="title">
							<a href="<?php the_permalink();?>">
								<h3><?php the_title();?></h3>
							</a>
						</div>
					</div>
				</article>

			<?php endwhile; wp_reset_query(); else: echo ''; endif; ?>
		</div>
	</div>
</aside>