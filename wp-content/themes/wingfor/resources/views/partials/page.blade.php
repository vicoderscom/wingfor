<div class="main-title">
	<a>
		<h1>{{ $title }}</h1>
	</a>
</div>

<div class="page-info">
	<?php the_content(); ?>
</div>