import meanmenu from 'jquery.meanmenu/jquery.meanmenu.js';
// using this import type for require third 3rd. For example:
// import Wow from 'wow.js';

// or you can specify the script file which you want to import
// import meanmenu from 'jquery.meanmenu/jquery.meanmenu.js';

export default {
  init() {
    // JavaScript to be fired on all pages



    //back to top
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 20) {
            $("#back-to-top").css("display", "block");
        } else {
            $("#back-to-top").css("display", "none");
        }
    });
    if($('#back-to-top').length){
        $("#back-to-top").on('click', function() {
           $('html, body').animate({
               scrollTop: $('html, body').offset().top
             }, 1000);
        });
    }

    //menu primary
    if ($('.menu').length) {
        $('.main-menu').meanmenu({
            meanScreenWidth: "992",
            meanMenuContainer: ".mobile-menu",
        });
    }

    //popup mobile
    $('.popup-plush').click(function(){
      $(this).parent().find('.popup-content').stop(true,false).slideToggle();
    });


  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
