<?php
get_header();

use NF\View\Facades\View;

$termObj = get_queried_object();
$termName = $termObj->name;

?>

<section class="product-list">
	<div class="container">
		<div class="row">

			<?php echo View::render('partials.sidebar'); ?>

			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 product">

				<?php echo View::render('partials.slide-all-page'); ?>
				<div class="main-title">
					<a>
						<h2>
							<?php echo $termName;?>
						</h2>
					</a>
				</div>
				<div class="product-content">
					<div class="row">
						<?php
							while (have_posts()) {
								the_post();

								$data = [
									'id' => get_the_ID(),
									'url' => get_permalink(),
									'title' => get_the_title(),
								];

								echo View::render('partials.test-tax', $data);
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();