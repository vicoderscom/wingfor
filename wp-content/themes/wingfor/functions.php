<?php

$app = require_once __DIR__ . '/bootstrap/app.php';

//include widget
include_once get_template_directory(). '/app/Widgets/show-post-category.php';
include_once get_template_directory(). '/app/Widgets/wp-statistics.php';

//lay img
function wingfor_get_thumbnail_url( $size = 'full' ) {
    global $post;
    if (has_post_thumbnail( $post->ID ) ) {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $size );
        return $image[0];
    }

    // use first attached image
    $images = get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
    if (!empty($images)) {
        $image = reset($images);
        $image_data = wp_get_attachment_image_src( $image->ID, $size );
        return $image_data[0];
    }

    // use no preview fallback
    if ( file_exists( get_template_directory().'/resources/assets/images/no-image.jpg' ) )
        return get_template_directory_uri().'/resources/assets/images/no-image.jpg';
    else
        return get_template_directory_uri().'/resources/assets/images/no-image.jpg';
}


//custom logo
class setting {
    function __construct(){
        add_theme_support( 'custom-logo' );
    }
}
new setting();

//Bộ lọc search
if( !is_admin() ) {
    function filter_search($query) {
        if ($query->is_search) {
            $query->set('post_type', array('products', 'post'));
        };
        return $query;
    };
    add_filter('pre_get_posts', 'filter_search');
}

//cat chuoi
function cut_string($str,$len,$more){
    if ($str=="" || $str==NULL) return $str;
    if (is_array($str)) return $str;
        $str = trim(strip_tags($str));
    if (strlen($str) <= $len) return $str;
        $str = substr($str,0,$len);
    if ($str != "") {
        if (!substr_count($str," ")) {
          if ($more) $str .= " ...";
          return $str;
        }
        while(strlen($str) && ($str[strlen($str)-1] != " ")) {
            $str = substr($str,0,-1);
        }
        $str = substr($str,0,-1);
        if ($more) $str .= " ...";
    }
    return $str;
}

//query sp cua category
function wingfor_custom_posttype_query($posttype, $taxonomy, $termId, $numPost){
	$qr =  new WP_Query( array(
			            'post_type' => $posttype,
			            'tax_query' => array(
			                                array(
			                                        'taxonomy' => $taxonomy,
			                                        'field' => 'id',
			                                        'terms' => $termId,
			                                        'operator'=> 'IN'
			                                 )),
			            'showposts'=>$numPost,
			            'order' => 'DESC',
			            'orderby' => 'date'
			     ) );
	return $qr;
}
//phan trang
function wingfor_custom_posttype_query_paged($posttype, $taxonomy, $termId, $paged, $size){
	$qr =  new WP_Query( array(
			            'post_type' => $posttype,
			            'tax_query' => array(
			                                array(
			                                        'taxonomy' => $taxonomy,
			                                        'field' => 'id',
			                                        'terms' => $termId,
			                                        'operator'=> 'IN'
			                                 )),
						'posts_per_page' => $size,
						'paged' => $paged,
						'order' => 'DESC',
						'orderby' => 'date'
			     ) );
	return $qr;
}

